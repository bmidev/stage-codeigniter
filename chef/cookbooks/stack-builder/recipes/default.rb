#
# Cookbook Name:: stack-builder
# Recipe:: default
#
# Copyright (C) 2015 Eric Stewart
#
# All rights reserved - Do Not Redistribute
#

# Install apt.
include_recipe 'apt'

# Install nginx.
include_recipe 'nginx'

# Install php
node.set['php']['directives']['cgi.fix_pathinfo'] = 0
include_recipe 'php'

# Install php curl extension
package "php5-curl" do
  action :install
end

# Install php gd extension
package "php5-gd" do
  action :install
end

# Install php mcrypt extension
package "php5-mcrypt" do
  action :install
end

# Install php memcached extension
package "php5-memcached" do
  action :install
end

# Install php mysqlnd extension
package "php5-mysqlnd" do
  action :install
end

# Install php xdebug extension.
package "php5-xdebug" do
  action :install
end

# Install php-fpm.
include_recipe 'php5-fpm::install'

# Fixes bug where Ubuntu doesn't correct create mcrypt.ini
link "/etc/php5/cli/conf.d/20-mcrypt.ini" do
  to "/etc/php5/mods-available/mcrypt.ini"
end
link "/etc/php5/fpm/conf.d/20-mcrypt.ini" do
  to "/etc/php5/mods-available/mcrypt.ini"
end

# Install main nginx configuration.
cookbook_file "/etc/nginx/nginx.conf" do
  source "nginx.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
end

# Install project server configuration.
cookbook_file "/etc/nginx/conf.d/project.conf" do
  source "project.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
end

cookbook_file "/etc/php5/fpm/php-fpm.conf" do
  source "php-fpm.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
  notifies :restart, "service[php5-fpm]", :delayed
end

cookbook_file "/etc/php5/fpm/pool.d/www.conf" do
  source "www.conf"
  owner 'root'
  group 'root'
  mode '0644'
  notifies :reload, "service[nginx]", :delayed
  notifies :restart, "service[php5-fpm]", :delayed
end

mysql_service 'default' do
  charset 'utf8'
  port '3306'
  version '5.6'
  initial_root_password 'root'
  action [:create, :start]
end

mysql2_chef_gem 'default' do
  action :install
end

mysql_database 'project_local' do
 connection(
   :host     => '127.0.0.1',
   :username => 'root',
   :password => 'root'
 )
 action :create
end